#!/bin/bash

export LORENE_DIR=`pwd`
make -j8


g++ -IExport/C++/Include -IC++/Include standalone_interpolator.cc -o standalone_interpolator -LLib/ -llorene_export -llorene -llorenef77 -lgfortran -lfftw3 -lgsl -lgslcblas -llapack -lblas
