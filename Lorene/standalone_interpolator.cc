/* (c) 2009 Erik Schnetter
 * (c) 2010 Frank Loeffler */

#include <cstdio>
#include <cstring>
#include <cassert>
#include <vector>
#include <ios>
#include <iostream>
#include <stdlib.h>

#include <bin_ns.h>
#include <unites.h>

using namespace std;

// define namespace here for old versions of Lorene that don't do so
namespace Lorene {}
using namespace Lorene;

#define CCTK_EQUALS(a,b) (strcmp((a),(b))==0)
#define CCTK_Equals(a,b) (strcmp((a),(b))==0)
char initial_shift[200];
char initial_lapse[200];
char initial_dtlapse[200];
char initial_dtshift[200];
char initial_data[200];

/*
static void set_dt_from_domega (int const npoints,
                                double const* const var,
                                double      * const dtvar,
                                double const& omega)
{
  vector<double> dxvar(npoints), dyvar(npoints);

  Diff_gv (cctkGH, 0, var, &dxvar[0], -1);
  Diff_gv (cctkGH, 1, var, &dyvar[0], -1);

#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {
    double const ephix = +y[i];
    double const ephiy = -x[i];
    double const dphi_var = ephix * dxvar[i] + ephiy * dyvar[i];
    dtvar[i] = omega * dphi_var;
  }
}
*/

int main(int argc, const char *argv[]) {
  sprintf(initial_lapse,"Meudon_Bin_NS");
  sprintf(initial_dtlapse,"zero");
  sprintf(initial_shift,  "zero");
  sprintf(initial_dtshift,"zero");
  sprintf(initial_data, "Meudon_Bin_NS");

  if(argc != 3) {
    fprintf(stderr,"Error, incorrect usage. Usage: ./standalone_interpolator [filename (resu.d)] [number of dest points]\n");
    exit(1);
  }
  
  printf ("Setting up LORENE Bin_NS initial data\n");

  // Meudon data are distributed in SI units (MKSA).  Here are some
  // conversion factors.
  // Be aware: these are the constants Lorene uses. They do differ from other
  // conventions, but they gave the best results in some tests.

  double const c_light  = Unites::c_si;      // speed of light [m/s]
  double const nuc_dens = Unites::rhonuc_si; // Nuclear density as used in Lorene units [kg/m^3]
  double const G_grav   = Unites::g_si;      // gravitational constant [m^3/kg/s^2]
  double const M_sun    = Unites::msol_si;   // solar mass [kg]

  // Cactus units in terms of SI units:
  // (These are derived from M = M_sun, c = G = 1, and using 1/M_sun
  // for the magnetic field)
  double const cactusM = M_sun;
  double const cactusL = cactusM * G_grav / pow(c_light,2);
  double const cactusT = cactusL / c_light;

  // Other quantities in terms of Cactus units
  double const coord_unit = cactusL / 1.0e+3;         // from km (~1.477)
  double const rho_unit   = cactusM / pow(cactusL,3); // from kg/m^3
  int keyerr = 0, anyerr = 0;

  printf ("Setting up coordinates\n");

  int const npoints = atoi(argv[2]); //cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  
  vector<double> x(npoints), y(npoints), z(npoints); // ETK COORDINATES
  x[0] = 1.0;
  y[0] = 1.0;
  z[0] = 1.0;
  vector<double> alp(npoints);
  vector<double> betax(npoints), betay(npoints), betaz(npoints);
  vector<double> gxx(npoints), gxy(npoints), gxz(npoints), gyy(npoints), gyz(npoints), gzz(npoints);
  vector<double> kxx(npoints), kxy(npoints), kxz(npoints), kyy(npoints), kyz(npoints), kzz(npoints);


  vector<double> xx(npoints), yy(npoints), zz(npoints); // LORENE COORDINATES, != ETK COORDINATES

  //Get EOS_Omni handle
  /*
  if (!(*init_eos_key = EOS_Omni_GetHandle(eos_table)))
    fprintf(stderr,"Cannot get initial eos handle, aborting...\n");
  
  fprintf(stderr,"Meudon_Bin_NS will use the %s equation of state.", eos_table);
  fprintf(stderr,"Meudon_Bin_NS will use the %d eos handle", *init_eos_key);
  */
 


#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {
    xx[i] = x[i] * coord_unit;
    yy[i] = y[i] * coord_unit;
    zz[i] = z[i] * coord_unit;
  }

  // --------------------------------------------------------------
  //   CHECKING FILE NAME EXISTENCE
  // --------------------------------------------------------------
  FILE *file;
  char filename[100];
  sprintf(filename,"%s",argv[1]);
  if ((file = fopen(filename, "r")) != NULL) 
     fclose(file);
  else {
    fprintf(stderr,
                 "File \"%s\" does not exist. ABORTING\n", filename);
    exit(1);
  }
  // Handle potentially different EOS table directory. LORENE recieves that via
  // environment variable
  /* IGNORE HYDRO DATA.
  if (strlen(eos_table_filepath) > 0) {
    if (setenv("LORENE_TABULATED_EOS_PATH", eos_table_filepath, 1)) {
      fprintf(stderr,"Unable to set environment variable LORENE_TABULATED_EOS_PATH\n");
      exit(1);

    }
  }
  */

printf( "Reading from file \"%s\"\n", filename);

  try {
  Bin_NS bin_ns (npoints, &xx[0], &yy[0], &zz[0], filename);

printf( "omega [rad/s]:       %g\n", bin_ns.omega);
printf( "dist [km]:           %g\n", bin_ns.dist);
printf( "dist_mass [km]:      %g\n", bin_ns.dist_mass);
printf( "mass1_b [M_sun]:     %g\n", bin_ns.mass1_b);
printf( "mass2_b [M_sun]:     %g\n", bin_ns.mass2_b);
printf( "mass_ADM [M_sun]:    %g\n", bin_ns.mass_adm);
printf( "L_tot [G M_sun^2/c]: %g\n", bin_ns.angu_mom);
printf( "rad1_x_comp [km]:    %g\n", bin_ns.rad1_x_comp);
printf( "rad1_y [km]:         %g\n", bin_ns.rad1_y);
printf( "rad1_z [km]:         %g\n", bin_ns.rad1_z);
printf( "rad1_x_opp [km]:     %g\n", bin_ns.rad1_x_opp);
printf( "rad2_x_comp [km]:    %g\n", bin_ns.rad2_x_comp);
printf( "rad2_y [km]:         %g\n", bin_ns.rad2_y);
printf( "rad2_z [km]:         %g\n", bin_ns.rad2_z);
printf( "rad2_x_opp [km]:     %g\n", bin_ns.rad2_x_opp);
  double K = bin_ns.kappa_poly1 * pow((pow(c_light, 6.0) /
             ( pow(G_grav, 3.0) * M_sun * M_sun *
               nuc_dens )),bin_ns.gamma_poly1-1.);
printf( "K [ET unit]:         %.15g\n", K);

  assert (bin_ns.np == npoints);

  printf ("Filling in Cactus grid points\n");

#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {

    if (CCTK_EQUALS(initial_lapse, "Meudon_Bin_NS")) { 
      alp[i] = bin_ns.nnn[i];
    }

    if (CCTK_EQUALS(initial_shift, "Meudon_Bin_NS")) { 
      betax[i] = -bin_ns.beta_x[i];
      betay[i] = -bin_ns.beta_y[i];
      betaz[i] = -bin_ns.beta_z[i];
    }

    if (CCTK_EQUALS(initial_data, "Meudon_Bin_NS")) {
      gxx[i] = bin_ns.g_xx[i];
      gxy[i] = bin_ns.g_xy[i];
      gxz[i] = bin_ns.g_xz[i];
      gyy[i] = bin_ns.g_yy[i];
      gyz[i] = bin_ns.g_yz[i];
      gzz[i] = bin_ns.g_zz[i];

      kxx[i] = bin_ns.k_xx[i] * coord_unit;
      kxy[i] = bin_ns.k_xy[i] * coord_unit;
      kxz[i] = bin_ns.k_xz[i] * coord_unit;
      kyy[i] = bin_ns.k_yy[i] * coord_unit;
      kyz[i] = bin_ns.k_yz[i] * coord_unit;
      kzz[i] = bin_ns.k_zz[i] * coord_unit;
    }

    printf("OUTPUT: %e %e %e %e %e %e\n",gxx[0],gxy[0],gxz[0],gyy[0],gyz[0],gzz[0]);
    
    if (CCTK_EQUALS(initial_data, "Meudon_Bin_NS")) {
      /* IGNORE ALL HYDRO:
      rho[i] = bin_ns.nbar[i] / rho_unit;
      if (!recalculate_eps)
        eps[i] = bin_ns.ener_spec[i];
      // Pressure from EOS_Omni call 
      if (CCTK_ActiveTimeLevelsVN(cctkGH, "HydroBase::temperature") > 0 &&
          CCTK_ActiveTimeLevelsVN(cctkGH, "HydroBase::Y_e") > 0)
      {
        EOS_Omni_press(*init_eos_key,recalculate_eps,eos_precision,1,&(rho[i]),&(eps[i]),
                       &(temperature[i]),&(Y_e[i]),&(press[i]),&keyerr,&anyerr);
      }
      else
      {
        EOS_Omni_press(*init_eos_key,recalculate_eps,eos_precision,1,&(rho[i]),&(eps[i]),
                       NULL,NULL,&(press[i]),&keyerr,&anyerr);
      }

      vel[i          ] = bin_ns.u_euler_x[i];
      vel[i+  npoints] = bin_ns.u_euler_y[i];
      vel[i+2*npoints] = bin_ns.u_euler_z[i];

      // Especially the velocity is set to strange values outside of the
      // matter region, so take care of this in the following way
      if (rho[i] < 1.e-20) {
        rho[i          ] = 1.e-20;
        vel[i          ] = 0.0;
        vel[i+  npoints] = 0.0;
        vel[i+2*npoints] = 0.0;
        eps[i          ] = K * pow(rho[i], bin_ns.gamma_poly1-1.) / (bin_ns.gamma_poly1-1.);
        press[i        ] = K * pow(rho[i], bin_ns.gamma_poly1);
      }
      */
    }

  } // for i

  {
    // Angular velocity
    double const omega = bin_ns.omega * cactusT;

    // These initial data assume a helical Killing vector field

    if (CCTK_EQUALS(initial_lapse, "Meudon_Bin_NS")) { 
      if (CCTK_EQUALS (initial_dtlapse, "Meudon_Bin_NS")) {
        printf ("Error: Nonzero time derivatives of lapse unsupported\n");
        exit(1);
        //set_dt_from_domega (npoints,alp, dtalp, omega);
      } else if (CCTK_EQUALS (initial_dtlapse, "none") or CCTK_EQUALS(initial_dtlapse,"zero")) {
        // do nothing
      } else {
        fprintf(stderr, "internal error\n");
        exit(1);
      }
    }

    if (CCTK_EQUALS(initial_shift, "Meudon_Bin_NS")) { 
      if (CCTK_EQUALS (initial_dtshift, "Meudon_Bin_NS")) {
        printf ("Error: Nonzero time derivatives of shift unsupported\n");
        exit(1);
        /*
        set_dt_from_domega (npoints,betax, dtbetax, omega);
        set_dt_from_domega (npoints,betay, dtbetay, omega);
        set_dt_from_domega (npoints,betaz, dtbetaz, omega);*/
        
      } else if (CCTK_EQUALS (initial_dtshift, "none") or CCTK_EQUALS(initial_dtshift,"zero")) {
        // do nothing
      } else {
        printf("internal error\n");
      }
    }
  }

  printf ("Done.\n");
  } catch (ios::failure e) {
    printf("Could not read initial data from file '%s': %s\n", filename, e.what());
  }
}

